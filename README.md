```


 __      _______  _          _____           _                    
 \ \    / /  __ \| |        |  __ \         | |                   
  \ \  / /| |__) | |  ______| |__) |_ _  ___| | ____ _  __ _  ___ 
   \ \/ / |  _  /| | |______|  ___/ _` |/ __| |/ / _` |/ _` |/ _ \
    \  /  | | \ \| |____    | |  | (_| | (__|   < (_| | (_| |  __/
     \/   |_|  \_\______|   |_|   \__,_|\___|_|\_\__,_|\__, |\___|
                                                        __/ |     
                                                       |___/      

```

VRL Package - A Carepackage for the BYOB Open Source project

You are having a hard time installing everything 100% perfect for BYOB?

Well me too. So i created a automatic script that does loads of fancy stuff that helps you.
Saves you loads of time googling and even remove you from needing to know Linux.

This is a 100% Linux-only repo, I will not help Windows users.

-vrl

| Social |
| ------ |
| [Discord](https://discord.gg/vrl) |
| [Twitter](https://twitter.com/vrlinux) |
| [Youtube](https://www.youtube.com/@_vr) |
| [Twitch](https://twitch.tv/vrl_here) |



# How to install?

#### Main way to install

```sh
curl -L https://gitlab.com/vrl/vrl-package/-/raw/master/auto_install/byob-installer.sh | bash
```

---

#### Currently not working...

```sh
curl -L http://install.vrl.sh | bash
```


###### _supplements for [BYOB](https://github.com/vrlnx/byob)_

```
Click the image under to get tutorial video.
```
[![Installment](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fcdn.cyberpunk.rs%2Fwp-content%2Fuploads%2F2018%2F10%2Fbuild_your_botnet_bg.jpg&f=1&nofb=1&ipt=fca4c94da2528978ff947797e4b79577c0a3ec00927717e8807fd4dd93af9e8b&ipo=images)](https://www.youtube.com/watch?v=vsXCvXQNNkI)
